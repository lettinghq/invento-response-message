# README #

### What is this repository for? ###

* Quickly display messages on the page from anywhere in your app
* Version 1.0.2


### How do I get set up? ###

* Install the package into your project

```
#!javascript

bower install invento-response-message --save
```


* Include the following line at the bottom of your index.html:

```
#!html

<script src="bower_components/invento-response-message/dist/js/response-message.js"></script>
```

* Include the following package in your app's module:

```
#!javascript

'invento-response-message'
```

* Add the following factory to your controller

```
#!javascript

ResponseMessage
```

* Call the factory from anywhere in your app like so:

```
#!javascript

ResponseMessage.success('I have been called!');
```


### Example ###

index.html

```
#!html

<html>
    <head>
        <script src="bower_components/invento-response-message/dist/js/response-message.js"></script>
    </head>
    <body>
        <div class="notification-wrapper" ng-controller="messages" ng-cloak>
            <div class="container-fluid" ng-repeat="(sectionType, section) in messages.sections">
                <div class="row">
                    <div class="col-xs-12 alert" ng-repeat="(messageUuid, message) in section.messages" ng-if="messages.length(sectionType)" ng-class="section.class" ng-click="messages.delete(sectionType, messageUuid)">
                        <span ng-bind-html="messages.renderHtml(message)"></span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
```

app.js

```
#!javascript

var app = angular.module('YourApp', ['invento-response-message']);

app.controller('messages', function($scope, ResponseMessage) {
    $scope.messages = ResponseMessage;

    ResponseMessage.success('Hello world!');
});
```