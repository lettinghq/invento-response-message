var responseMessage = angular.module('invento-response-message', []);

responseMessage.factory('ResponseMessage', function($timeout, $sce, $location, $localStorage) {
    var ResponseMessage = {};

    ResponseMessage.TIMEOUT_RESPONSE = 'The request timed out, please try again';
    ResponseMessage.UNKNOWN_RESPONSE = 'Whoops! Something went wrong, please try again';

    /*
     * Final object used by the view
     */
    ResponseMessage.sections = {
        success : {
            timer       : 11100,
            class       : 'alert-success',
            messages    : {},
        },
        warning : {
            timer       : 16100,
            class       : 'alert-warning',
            messages    : {},
        },
        error   : {
            timer       : 21100,
            class       : 'alert-warning',
            messages    : {},
        },
    };

    /*
     * Displays a success message
     *
     * @param {string} "message" The message type being evaluated
     */
    ResponseMessage.success = function(message) {
        ResponseMessage.filter('success', ResponseMessage.deserialize(message, false));
    };

    /*
     * Displays a warning message
     *
     * @param {string} "message" The message type being evaluated
     */
    ResponseMessage.warning = function(message) {
        ResponseMessage.filter('warning', ResponseMessage.deserialize(message, false));
    };

    /*
     * Displays an error message
     *
     * @param {string}  "message" The message type being evaluated
     * @param {bool}    "fromAPI" Whether or not the message needs to be filtered by response type or not
     */
    ResponseMessage.error = function(message, fromAPI) {
        ResponseMessage.filter('error', ResponseMessage.deserialize(message, fromAPI));
    };

    /*
     * Filters through the array of messages and pushes them into the message array individually
     *
     * @param {string} "type"           The section type being evaluated
     * @param {array} "filteredMessage" The array of messages
     */
    ResponseMessage.filter = function(type, filteredMessage) {
        angular.forEach(filteredMessage, function(potentiallyMultipleMessages, potentiallyMultipleMessagesKey) {
            if (angular.isObject(potentiallyMultipleMessages)) {
                angular.forEach(potentiallyMultipleMessages, function(singleMessage, singleMessageKey) {
                    if (!ResponseMessage.checkExists(type, singleMessage)) {
                        ResponseMessage.display(type, singleMessage);
                    }
                });
            } else {
                if (!ResponseMessage.checkExists(type, potentiallyMultipleMessages)) {
                    ResponseMessage.display(type, potentiallyMultipleMessages);
                }
            }
        });
    };

    /*
     * Checks whether the message is already being displayed or not
     *
     * @param {mixed}   "message" The message(s) being evaluated
     * @return {string}
     */
    ResponseMessage.checkExists = function(type, message) {
        for (var key in ResponseMessage.sections[type].messages) {
            if (ResponseMessage.sections[type].messages[key].toLowerCase() == message.toLowerCase()) return true;
        }

        return false;
    };

    /*
     * Converts the given message(s) into an array
     *
     * @param {mixed}   "message" The message(s) being evaluated
     * @param {bool}    "fromAPI" Whether the message is an error being returned from an API or not
     * @return {array}
     */
    ResponseMessage.deserialize = function(message, fromAPI) {
        try {
            return fromAPI ? angular.fromJson(ResponseMessage.filterByStatus(message)) : angular.fromJson(message);
        } catch (e) {
            return fromAPI ? [ResponseMessage.filterByStatus(message)] : [message];
        }
    };

    /*
     * Filters what should be returned by response status
     *
     * @param {string}  "message" The message being evaluated
     * @return {string}
     */
    ResponseMessage.filterByStatus = function(message) {
        if (message.status === 401) {
            $localStorage.access_attempt_response = message.data.response.data;
            $location.path('/login');
        } else if (message.status === 403) {
            $location.path('/logout');
            return message.data.response;
        } else if (message.status === 504) {
            return ResponseMessage.TIMEOUT_RESPONSE;
        } else {
            if (!message.data || message.data === null || !message.data.response) {
                return ResponseMessage.UNKNOWN_RESPONSE;
            } else {
                var messageToShow = message.data.response.errors ? message.data.response.errors : message.data.response;

                if (messageToShow.toLowerCase().indexOf('sqlstate') > -1) {
                    messageToShow = ResponseMessage.UNKNOWN_RESPONSE;
                }

                return messageToShow;
            }           
        }
    };

    /*
     * Pushes the message into the given section and scheduled it to be automatically removed
     *
     * @param {string} "type"       The section type being evaluated
     * @param {string} "message"    The message being pushed to the view
     */
    ResponseMessage.display = function(type, message) {
        var uuid = ResponseMessage.uuid();

        ResponseMessage.sections[type].messages[uuid] = message;

        $timeout(function(items) {
            ResponseMessage.delete(items[0], items[1]);
        }, ResponseMessage.sections[type].timer, true, [type, uuid]);
    };

    /*
     * Generates a random 5 character string
     *
     * @return {string} "uuid" The unique identifier value
     */
    ResponseMessage.uuid = function() {
        return Math.random().toString(36).substr(2, 5);
    };

    /*
     * Deletes the message from the appropriate message object
     *
     * @param {string} "type" The section type being evaluated
     * @param {string} "uuid" The unique identifier the message is stored under
     */
    ResponseMessage.delete = function(type, uuid) {
        if (ResponseMessage.sections[type].messages[uuid]) {
            delete ResponseMessage.sections[type].messages[uuid];
        }
    };

    /*
     * Checks if there are any messages in the given section
     *
     * @param {string} "type" The section type being evaluated
     * @return {bool}
     */
    ResponseMessage.length = function(type) {
        return Object.keys(ResponseMessage.sections[type].messages).length > 0;
    };

    /*
     * Allows a message with html in it to be rendered properly
     *
     * @param {string} "html" The message being evaluated
     * @return {string}
     */
    ResponseMessage.renderHtml = function(html) {
        return $sce.trustAsHtml(html);
    };

    return ResponseMessage;
});